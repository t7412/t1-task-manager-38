package ru.t1.chubarov.tm.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Role;

public final class User extends AbstractModel {

    @Nullable
    private String login;
    @Nullable
    private String passwordHash;
    @Nullable
    private String email;
    @Nullable
    private String firstName;
    @Nullable
    private String lastName;
    @Nullable
    private String middleName;
    @NotNull
    private Role role = Role.USUAL;
    @NotNull
    private Boolean locked = false;

    @NotNull
    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(@NotNull Boolean locked) {
        this.locked = locked;
    }

    @Nullable
    public String getLogin() {
        return login;
    }

    @Nullable
    public String getPasswordHash() {
        return passwordHash;
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    @Nullable
    public String getFirstName() {
        return firstName;
    }

    @Nullable
    public String getLastName() {
        return lastName;
    }

    @Nullable
    public String getMiddleName() {
        return middleName;
    }

    @NotNull
    public Role getRole() {
        return role;
    }

    public void setLogin(@Nullable String login) {
        this.login = login;
    }

    public void setPasswordHash(@Nullable String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public void setEmail(@Nullable String email) {
        this.email = email;
    }

    public void setFirstName(@Nullable String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(@Nullable String lastName) {
        this.lastName = lastName;
    }

    public void setMiddleName(@Nullable String middleName) {
        this.middleName = middleName;
    }

    public void setRole(@NotNull Role role) {
        this.role = role;
    }

}
