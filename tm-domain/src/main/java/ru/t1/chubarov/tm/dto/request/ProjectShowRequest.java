package ru.t1.chubarov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.ProjectSort;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectShowRequest extends AbstractUserRequest {

    @Nullable
    private ProjectSort sort;

    public ProjectShowRequest(@Nullable final ProjectSort sort, @Nullable final String token) {
        super(token);
        this.sort = sort;
    }

}
