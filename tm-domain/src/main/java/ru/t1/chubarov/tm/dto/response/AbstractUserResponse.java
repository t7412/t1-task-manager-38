package ru.t1.chubarov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private User user;

    @Nullable
    private String token;

    @Nullable
    private Throwable throwable;

    public AbstractUserResponse(@Nullable final User user, @Nullable final String token, @Nullable final Throwable throwable) {
        this.user = user;
        this.token = token;
        this.throwable = throwable;
    }

    public AbstractUserResponse(@Nullable final Throwable throwable) {
        this.throwable = throwable;
    }

    public AbstractUserResponse(@Nullable final String token) {
        this.token = token;
    }

    public AbstractUserResponse(@Nullable final User user) {
        this.user = user;
    }
}
