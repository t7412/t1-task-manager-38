package ru.t1.chubarov.tm.exception.user;

import ru.t1.chubarov.tm.exception.field.AbstractFieldException;

public final class AccessDeniedException extends AbstractFieldException {

    public AccessDeniedException() {
        super("Error. Access denied.");
    }

}
