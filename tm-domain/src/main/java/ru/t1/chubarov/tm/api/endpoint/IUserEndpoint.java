package ru.t1.chubarov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.request.*;
import ru.t1.chubarov.tm.dto.response.*;
import javax.validation.constraints.NotNull;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IUserEndpoint extends IEndpoint {

    @NotNull
    String NAME = "UserEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IUserEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @NotNull
    @SneakyThrows
    @WebMethod
    UserLockResponse lockUser(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserLockRequest request);

    @NotNull
    @SneakyThrows
    @WebMethod
    UserUnlockResponse unlockUser(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserUnlockRequest request);

    @Nullable
    @SneakyThrows
    @WebMethod
    UserRemoveResponse removeUser(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserRemoveRequest request);

    @NotNull
    @SneakyThrows
    @WebMethod
    UserUpdateProfileResponse updateUserProfile(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserUpdateProfileRequest request);

    @NotNull
    @SneakyThrows
    @WebMethod
    UserChangePasswordResponse changeUserPassword(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserChangePasswordRequest request);

    @NotNull
    @SneakyThrows
    @WebMethod
    UserRegistryResponse registryUser(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserRegistryRequest request);

}
