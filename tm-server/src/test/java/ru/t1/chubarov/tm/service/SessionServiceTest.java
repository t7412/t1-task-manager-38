package ru.t1.chubarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.api.service.ISessionService;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.model.Session;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class SessionServiceTest {

    private static final int NUMBER_OF_ENTRIES = 2;

    @NotNull
    final IDatabaseProperty databaseProperty = new PropertyService();

    @NotNull
    final IConnectionService connectionService = new ConnectionService(databaseProperty);

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private final String firstSessionId = UUID.randomUUID().toString();

    @NotNull
    private final String userUserId = UUID.randomUUID().toString();

    @NotNull
    private final String userAdminId = UUID.randomUUID().toString();

    @Before
    public void initTest() throws Exception {
        sessionService = new SessionService(connectionService);
        sessionList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            session.setDate(new Date());
            if (i <= 1) {
                session.setId(firstSessionId);
                session.setUserId(userAdminId);
            } else {
                session.setId(UUID.randomUUID().toString());
                session.setUserId(userUserId);
            }
            sessionList.add(session);
        }
        sessionService.set(sessionList);
    }

    @After
    public void finish() throws Exception {
        sessionList = new ArrayList<>();
        sessionService.set(sessionList);
    }

    @Test
    public void testSize() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionService.getSize());
    }

    @SneakyThrows
    @Test
    public void testAdd() {
        sessionService.add(new Session());
        sessionService.add(new Session());
        Assert.assertEquals(NUMBER_OF_ENTRIES + 2, sessionService.getSize());
    }

    @Test
    public void testCreateNegative() {
        @Nullable final Session session = null;
        Assert.assertThrows(ModelNotFoundException.class, () -> sessionService.add(session));
    }

    @SneakyThrows
    @Test
    public void testExistsById() {
        Assert.assertTrue(sessionService.existsById(userAdminId,firstSessionId));
        Assert.assertFalse(sessionService.existsById(userUserId,firstSessionId));
        Assert.assertFalse(sessionService.existsById(userUserId,""));
        Assert.assertFalse(sessionService.existsById(userUserId,null));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.existsById("",firstSessionId));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.existsById(null,firstSessionId));
    }

    @SneakyThrows
    @Test
    public void testFindOneByIndex() {
        @Nullable final String userId = sessionList.get(0).getUserId();
        Assert.assertEquals(firstSessionId, sessionService.findOneByIndex(userId, 1).getId());
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(NUMBER_OF_ENTRIES + 1).getId());
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(-1).getId());
        Assert.assertEquals(firstSessionId, sessionService.findOneByIndex(userId, 1).getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneByIndex("", 0).getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneByIndex(null, 0).getId());
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(userId, NUMBER_OF_ENTRIES + 1).getId());
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(userId, -1).getId());
    }

    @SneakyThrows
    @Test
    public void testFindOneById() {
        @Nullable final String userId = sessionList.get(0).getUserId();
        Assert.assertEquals(firstSessionId, sessionService.findOneById(firstSessionId).getId());
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.findOneById("").getId());
        Assert.assertEquals(firstSessionId, sessionService.findOneById(userId, firstSessionId).getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneById("", firstSessionId).getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneById(null, firstSessionId).getId());
    }

    @Test
    public void testFindAll() throws Exception {
        @Nullable final List<Session> actualSessionList = sessionService.findAll();
        Assert.assertEquals(2, actualSessionList.size());
        @Nullable final List<Session> userSessionList = sessionService.findAll(userUserId);
        Assert.assertEquals(1, userSessionList.size());
    }

    @SneakyThrows
    @Test
    public void testRemove() {
        sessionService.remove(sessionList.get(1));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionService.getSize());
    }

    @Test
    public void testRemoveAll() throws Exception {
        sessionService.removeAll(userAdminId);
        Assert.assertEquals(0, sessionService.getSize(userAdminId));
    }

    @SneakyThrows
    @Test
    public void testRemoveOneById() {
        sessionService.removeOneById(userUserId, sessionList.get(1).getId());
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionService.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemoveOneByIndex() {
        sessionService.removeOneByIndex(userUserId, 1);
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionService.getSize());
    }

}
