package ru.t1.chubarov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.repository.IProjectRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.enumerated.ProjectSort;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.service.ConnectionService;
import ru.t1.chubarov.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final String userIdFirst = UUID.randomUUID().toString();

    @NotNull
    private final String userIdSecond = UUID.randomUUID().toString();

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    final IDatabaseProperty databaseProperty = new PropertyService();

    @NotNull
    final IConnectionService connectionService = new ConnectionService(databaseProperty);

    @SneakyThrows
    @Before
    public void initRepository() {
        System.out.println("Project. Start Before.");
        projectList = new ArrayList<>();
        projectRepository = new ProjectRepository(connectionService.getConnection());
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("project_" + i);
            project.setDescription("project description_" + i);
            if (i <= 5) project.setUserId(userIdFirst);
            else project.setUserId(userIdSecond);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @After
    public void finish() throws Exception {
        System.out.println("Project Start After.");
        projectList.clear();
        projectRepository.removeAll(userIdFirst);
        projectRepository.removeAll(userIdSecond);
    }

    @SneakyThrows
    @Test
    public void testAdd() {
        int numberOfProject = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Project Description";
        @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project = projectRepository.add(userId, project);
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
        @NotNull final Project actualproject = projectRepository.findOneByIndex(numberOfProject);
        Assert.assertNotNull(actualproject);
        Assert.assertEquals(project.getId(), actualproject.getId());
        Assert.assertEquals(userId, actualproject.getUserId());
        Assert.assertEquals(name, actualproject.getName());
        Assert.assertEquals(description, actualproject.getDescription());
    }

    @SneakyThrows
    @Test
    public void testAddUserNegative() {
        int numberOfProject = NUMBER_OF_ENTRIES;
        @NotNull final String userId = null;
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Project Description";
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemoveAllUser() {
        projectRepository.removeAll(userIdFirst);
        Assert.assertEquals(5, projectRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testFindSort() {
        @NotNull final ProjectSort sort = ProjectSort.toSort("BY_NAME");
        @NotNull final List<Project> actualProjectList = projectRepository.findAll(userIdFirst, sort.getComparator());
        Assert.assertEquals(5, actualProjectList.size());

    }

    @Test
    public void testFindAllUser() throws Exception {
        @NotNull final List<Project> actualProjectList = projectRepository.findAll(userIdFirst);
        Assert.assertEquals(5, actualProjectList.size());
    }

    @SneakyThrows
    @Test
    public void testFindOneById() {
        int numberOfProject = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project";
        @NotNull Project project = new Project();
        project.setName(name);
        project = projectRepository.add(userId, project);
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), projectRepository.findOneById(userId, project.getId()).getId());
    }

    @SneakyThrows
    @Test
    public void testRemoveOne() {
        projectRepository.remove(userIdFirst, projectList.get(1));
        Assert.assertEquals(9, projectRepository.getSize());
        Assert.assertEquals(4, projectRepository.getSize(userIdFirst));
    }

    @SneakyThrows
    @Test
    public void testRemoveOneById() {
        int numberOfProject = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final Project project = new Project();
        project.setName("Test Project");

        @NotNull final Project project_new = projectRepository.add(userId, project);
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
        @NotNull final Project actualProjectIndex = projectRepository.findOneById(userId, project_new.getId());
        Assert.assertNotNull(actualProjectIndex);
        Assert.assertNull(projectRepository.removeOneById(userId, "fail_test_id"));
        projectRepository.removeOneById(userId, actualProjectIndex.getId());
        Assert.assertEquals(numberOfProject - 1, projectRepository.getSize());

        projectRepository.add(userId, project);
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
        projectRepository.removeOneById(userId, project_new.getId());
        Assert.assertEquals(numberOfProject - 1, projectRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemoveOneByIndex() {
        int numberOfProject = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final Project project = new Project();
        project.setName("Test Project");

        @NotNull final Project project_new = projectRepository.add(userId, project);
        Assert.assertNotNull(project_new);
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
        projectRepository.removeOneByIndex(userId, numberOfProject);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());

        projectRepository.add(userId, project);
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
        Assert.assertNotNull(projectRepository.removeOneByIndex(userId, numberOfProject - 1));
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testExistById() {
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project";
        @NotNull final Project project = new Project();
        project.setName(name);
        @NotNull final Project actualProject = projectRepository.add(userId, project);
        Assert.assertEquals(true, projectRepository.existsById(actualProject.getId()));
        Assert.assertEquals(true, projectRepository.existsById(userId, actualProject.getId()));
        Assert.assertEquals(false, projectRepository.existsById("1111"));
        Assert.assertEquals(false, projectRepository.existsById(userId, "1111"));
        Assert.assertEquals(false, projectRepository.existsById("userId-123-4", actualProject.getId()));
    }

}
