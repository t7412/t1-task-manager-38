package ru.t1.chubarov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.repository.ITaskRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.enumerated.TaskSort;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.service.ConnectionService;
import ru.t1.chubarov.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final String userIdFirst = UUID.randomUUID().toString();

    @NotNull
    private final String userIdSecond = UUID.randomUUID().toString();

    @NotNull
    private final String taskProjectId = UUID.randomUUID().toString();

    @NotNull
    private List<Task> taskList;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    final IDatabaseProperty databaseProperty = new PropertyService();

    @NotNull
    final IConnectionService connectionService = new ConnectionService(databaseProperty);

    @SneakyThrows
    @Before
    public void initRepository() {
        System.out.println("Task. Start Before.");
        taskList = new ArrayList<>();
        taskRepository = new TaskRepository(connectionService.getConnection());
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("task_" + i);
            task.setProjectId(taskProjectId);
            task.setDescription("task description_" + i);
            if (i <= 5) task.setUserId(userIdFirst);
            else task.setUserId(userIdSecond);
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @After
    public void finish() throws Exception {
        System.out.println("Start After.");
        taskList.clear();
        taskRepository.removeAll(userIdFirst);
        taskRepository.removeAll(userIdSecond);
    }

    @SneakyThrows
    @Test
    public void testAdd() {
        int numberOfTask = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Task";
        @NotNull final String description = "Test Task Description";
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
        Assert.assertEquals(numberOfTask, taskRepository.getSize());
        @NotNull final Task actualtask = taskRepository.findOneByIndex(userId, numberOfTask - 1);
        Assert.assertNotNull(actualtask);
        Assert.assertEquals(userId, actualtask.getUserId());
        Assert.assertEquals(name, actualtask.getName());
        Assert.assertEquals(description, actualtask.getDescription());
    }

    @SneakyThrows
    @Test
    public void testAddUserNegative() {
        int numberOfTask = NUMBER_OF_ENTRIES;
        @NotNull final String userId = null;
        @NotNull final String name = "Test Task";
        @NotNull final String description = "Test Task Description";
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
        Assert.assertEquals(numberOfTask, taskRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemoveAllUser() {
        taskRepository.removeAll(userIdFirst);
        Assert.assertEquals(5, taskRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testFindSort() {
        @NotNull final TaskSort sort = TaskSort.toSort("BY_NAME");
        @NotNull final List<Task> actualTaskList = taskRepository.findAll(userIdFirst,sort.getComparator());
        Assert.assertEquals(5, actualTaskList.size());

    }

    @Test
    public void testFindAllUser() throws Exception {
        @NotNull final List<Task> actualTaskList = taskRepository.findAll(userIdFirst);
        Assert.assertEquals(5, actualTaskList.size());
    }

    @SneakyThrows
    @Test
    public void testFindOneById() {
        int numberOfTask = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Task";
        @NotNull final Task task = new Task();
        task.setName(name);
        taskRepository.add(userId, task);
        Assert.assertEquals(numberOfTask, taskRepository.getSize());
        @NotNull final Task actualTaskIndex = taskRepository.findOneByIndex(userId, numberOfTask - 1);
        Assert.assertNotNull(actualTaskIndex);
        @NotNull final String projecyId = actualTaskIndex.getId();
        Assert.assertNotNull(projecyId);
        Assert.assertEquals(projecyId, taskRepository.findOneById(userId, projecyId).getId());
    }

    @SneakyThrows
    @Test
    public void testFindByProjectId() {
        Assert.assertEquals(1, taskRepository.findAllByProjectId(userIdFirst,taskProjectId).size());
        Assert.assertEquals(0, taskRepository.findAllByProjectId(userIdFirst,"fail_roject_id").size());
    }

    @SneakyThrows
    @Test
    public void testRemoveOne() {
        taskRepository.remove(userIdFirst, taskList.get(1));
        Assert.assertEquals(9, taskRepository.getSize());
        Assert.assertEquals(4, taskRepository.getSize(userIdFirst));
    }

    @SneakyThrows
    @Test
    public void testRemoveOneById() {
        int numberOfTask = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final Task task = new Task();
        task.setName("Test Task");

        taskRepository.add(userId, task);
        Assert.assertNull(taskRepository.removeOneById(userId,"fail_test_id"));
        Assert.assertEquals(numberOfTask, taskRepository.getSize());
        @NotNull final Task actualTaskIndex = taskRepository.findOneByIndex(userId,numberOfTask - 1);
        Assert.assertNotNull(actualTaskIndex);
        taskRepository.removeOneById(userId, actualTaskIndex.getId());
        Assert.assertEquals(numberOfTask - 1, taskRepository.getSize());

        taskRepository.add(userId, task);
        Assert.assertEquals(numberOfTask, taskRepository.getSize());
        taskRepository.removeOneById(userId, taskRepository.findOneByIndex(userId, numberOfTask - 1).getId());
        Assert.assertEquals(numberOfTask - 1, taskRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemoveOneByIndex() {
        int numberOfTask = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final Task task = new Task();
        task.setName("Test Task");

        taskRepository.add(userId, task);
        Assert.assertEquals(numberOfTask, taskRepository.getSize());
        taskRepository.removeOneByIndex(userId,numberOfTask - 1);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());

        taskRepository.add(userId, task);
        Assert.assertEquals(numberOfTask, taskRepository.getSize());
        Assert.assertNotNull(taskRepository.removeOneByIndex(userId, numberOfTask - 1));
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testExistById() {
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Task";
        @NotNull final Task task = new Task();
        task.setName(name);
        @NotNull final Task actualTask = taskRepository.add(userId, task);
        Assert.assertEquals(true, taskRepository.existsById(userId, actualTask.getId()));
        Assert.assertEquals(false, taskRepository.existsById(userId, "1111"));
        Assert.assertEquals(false, taskRepository.existsById("userId-123-4", actualTask.getId()));
    }

}
