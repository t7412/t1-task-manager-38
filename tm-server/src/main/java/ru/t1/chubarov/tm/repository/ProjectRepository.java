package ru.t1.chubarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.DBConstants;
import ru.t1.chubarov.tm.api.repository.IProjectRepository;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return DBConstants.TABLE_PROJECT;
    }

    @NotNull
    @Override
    public Project fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(row.getString(DBConstants.COLUMN_ID));
        project.setName(row.getString(DBConstants.COLUMN_NAME));
        project.setCreated(row.getTimestamp(DBConstants.COLUMN_CREATED));
        project.setDescription(row.getString(DBConstants.COLUMN_DESCRIPTION));
        project.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        project.setStatus(Status.valueOf(row.getString(DBConstants.COLUMN_STATUS)));
        return project;
    }

    @NotNull
    @Override
    public Project add(@NotNull final Project project) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s) VALUES (?,?,?,?,?,?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_NAME, DBConstants.COLUMN_CREATED,
                DBConstants.COLUMN_DESCRIPTION, DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_STATUS
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setString(2, project.getName());
            statement.setTimestamp(3, new Timestamp(project.getCreated().getTime()));
            statement.setString(4, project.getDescription());
            statement.setString(5, project.getUserId());
            statement.setString(6, project.getStatus().name());
            statement.executeUpdate();
        }
        return project;
    }

    @Override
    public Project update(@NotNull final Project project) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?",
                getTableName(), DBConstants.COLUMN_NAME, DBConstants.COLUMN_DESCRIPTION,
                DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_STATUS
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setString(3, project.getUserId());
            statement.setString(4, project.getStatus().toString());
            statement.executeUpdate();
        }
        return project;
    }

}
