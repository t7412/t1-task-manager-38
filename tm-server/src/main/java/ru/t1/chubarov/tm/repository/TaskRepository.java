package ru.t1.chubarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.DBConstants;
import ru.t1.chubarov.tm.api.repository.ITaskRepository;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return DBConstants.TABLE_TASK;
    }

    @NotNull
    @Override
    public Task fetch(@NotNull ResultSet row) throws Exception {
        @NotNull final Task task = new Task();
        task.setId(row.getString(DBConstants.COLUMN_ID));
        task.setName(row.getString(DBConstants.COLUMN_NAME));
        task.setCreated(row.getTimestamp(DBConstants.COLUMN_CREATED));
        task.setDescription(row.getString(DBConstants.COLUMN_DESCRIPTION));
        task.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        task.setProjectId(row.getString(DBConstants.COLUMN_PROJECT_ID));
        task.setStatus(Status.valueOf(row.getString(DBConstants.COLUMN_STATUS)));
        return task;
    }

    @NotNull
    @Override
    public Task add(@NotNull Task task) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s) VALUES (?,?,?,?,?,?,?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_NAME, DBConstants.COLUMN_CREATED,
                DBConstants.COLUMN_DESCRIPTION, DBConstants.COLUMN_USER_ID,
                DBConstants.COLUMN_PROJECT_ID, DBConstants.COLUMN_STATUS
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setString(2, task.getName());
            statement.setTimestamp(3, new Timestamp(task.getCreated().getTime()));
            statement.setString(4, task.getDescription());
            statement.setString(5, task.getUserId());
            statement.setString(6, task.getProjectId());
            statement.setString(7, task.getStatus().name());
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? AND %s = ? LIMIT 1", getTableName(), DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_PROJECT_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
            return result;
        }
    }

    @Override
    public Task update(@NotNull final Task task) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?",
                getTableName(), DBConstants.COLUMN_NAME, DBConstants.COLUMN_DESCRIPTION,
                DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_STATUS
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getUserId());
            statement.setString(4, task.getStatus().toString());
            statement.executeUpdate();
        }
        return task;
    }

}
