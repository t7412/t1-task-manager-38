package ru.t1.chubarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnerRepository<Task> {

    @Nullable
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

    Task update(@NotNull Task task) throws Exception;

}
