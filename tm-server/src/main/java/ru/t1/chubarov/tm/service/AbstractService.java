package ru.t1.chubarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.IRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.api.service.IService;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.IndexIncorrectException;
import ru.t1.chubarov.tm.model.AbstractModel;

import java.sql.Connection;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    protected AbstractService(@Nullable final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    protected abstract IRepository<M> getRepository(@NotNull final Connection connection);

    @NotNull
    @Override
    public M add(@Nullable final M model) throws Exception {
        if (model == null) throw new ModelNotFoundException();
        @NotNull M object;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            object = repository.add(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return object;
    }

    @Override
    @NotNull
    public Collection<M> add(@NotNull final Collection<M> models) throws Exception {
        @NotNull Collection<M> objects;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            objects = repository.add(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return objects;
    }

    @Override
    @NotNull
    public Collection<M> set(@NotNull final Collection<M> models) throws Exception {
        @NotNull Collection<M> objects;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            objects = repository.set(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return objects;
    }

    @Override
    public void clear() throws Exception {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll() throws Exception {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws Exception {
        if (comparator == null) return findAll();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll(comparator);
        }
    }

    @NotNull
    @Override
    public M remove(@Nullable final M model) throws Exception {
        if (model == null) throw new ModelNotFoundException();
        @NotNull M object;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            object = repository.remove(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return object;
    }

    @NotNull
    @Override
    public M findOneById(@NotNull final String id) throws Exception {
        if (id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            @NotNull final M model = repository.findOneById(id);
            if (model == null) throw new ModelNotFoundException();
            return model;
        }
    }

    @NotNull
    @Override
    public M findOneByIndex(@NotNull final Integer index) throws Exception {
        if (index < 0) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            if (index >= repository.getSize()) throw new IndexIncorrectException();
            @NotNull final M model = repository.findOneByIndex(index);
            if (model == null) throw new ModelNotFoundException();
            return model;
        }
    }

    @Override
    public boolean existsById(@NotNull final String id) throws Exception {
        if (id.isEmpty()) return false;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.existsById(id);
        }
    }

    @Override
    public int getSize() throws Exception {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.getSize();
        }
    }

}
