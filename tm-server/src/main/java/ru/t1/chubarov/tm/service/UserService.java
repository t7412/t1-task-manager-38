package ru.t1.chubarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.IProjectRepository;
import ru.t1.chubarov.tm.api.repository.ITaskRepository;
import ru.t1.chubarov.tm.api.repository.IUserRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.api.service.IPropertyService;
import ru.t1.chubarov.tm.api.service.IUserService;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.exception.user.UserNotFoundException;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.repository.UserRepository;
import ru.t1.chubarov.tm.util.HashUtil;

import java.sql.Connection;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectRepository projectRepository;
    @NotNull
    private final ITaskRepository taskRepository;
    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IPropertyService propertyService,
                       @Nullable final IConnectionService connectionService,
                       @Nullable final ITaskRepository taskRepository,
                       @Nullable final IProjectRepository projectRepository) {
        super(connectionService);
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.propertyService = propertyService;
    }

    @NotNull
    public IUserRepository getRepository(@NotNull final Connection connection) {
        return new UserRepository(connection);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException(login, email);
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        user.setRole(Role.USUAL);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.isLoginExist(login);
        }
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) return false;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.isEmailExist(email);
        }
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findByLogin(login);
        }
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findByEmail(email);
        }
    }

    @Nullable
    @Override
    public User removeOne(@Nullable final User model) throws Exception {
        if (model == null) return null;
        @Nullable final User user = super.remove(model);
        @Nullable final String userId = user.getId();
        taskRepository.removeAll(userId);
        projectRepository.removeAll(userId);
        return user;
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        return removeOne(user);
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException("login", email);
        @Nullable final User user = findByEmail(email);
        if (user == null) return null;
        return removeOne(user);
    }

    @NotNull
    @Override
    public User setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.update(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User updateUser(@Nullable final String id,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final User user = findOneById(id);
        user.setFirstName((firstName == null) ? "" : firstName);
        user.setLastName((lastName == null) ? "" : lastName);
        user.setMiddleName((middleName == null) ? "" : middleName);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.update(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.update(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.update(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
