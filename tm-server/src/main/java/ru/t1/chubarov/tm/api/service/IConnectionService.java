package ru.t1.chubarov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;

public interface IConnectionService {

    @SneakyThrows
    @NotNull
    Connection getConnection();

}
