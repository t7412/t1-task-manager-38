package ru.t1.chubarov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.repository.ISessionRepository;
import ru.t1.chubarov.tm.api.repository.IUserOwnerRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.api.service.ISessionService;
import ru.t1.chubarov.tm.model.Session;
import ru.t1.chubarov.tm.repository.SessionRepository;

import java.sql.Connection;

public class SessionService extends AbstractUserOwnerService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected IUserOwnerRepository<Session> getRepository(@NotNull Connection connection) {
        return new SessionRepository(connection);
    }

}
