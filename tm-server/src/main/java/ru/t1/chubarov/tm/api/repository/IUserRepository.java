package ru.t1.chubarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User update(@NotNull User user) throws Exception;

    @NotNull
    User create(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @Nullable String email) throws Exception;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @Nullable Role role) throws Exception;

    @Nullable
    User findByLogin(@NotNull String login) throws Exception;

    @Nullable
    User findByEmail(@NotNull String email) throws Exception;

    boolean isLoginExist(@NotNull String login) throws Exception;

    boolean isEmailExist(@NotNull String email) throws Exception;

}
