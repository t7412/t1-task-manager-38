package ru.t1.chubarov.tm.api.repository;

import ru.t1.chubarov.tm.model.Session;

public interface ISessionRepository extends IUserOwnerRepository<Session> {

}
